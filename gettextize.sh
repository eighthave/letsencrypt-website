#!/bin/sh -ex

export PERLLIB=/home/hans/code/mquinson/po4a/lib
export PATH=/home/hans/code/mquinson/po4a:$PATH

git -C po/ clean -fdx
git -C content/ clean -fdx
git checkout -- content/[acdf-z]*/docs/ content/es/docs/
git checkout -- content/[acdf-z]*/*.* content/es/*.*
rm -f gettextization.failed.po

docs_untranslated=""
for locale in de es fr id ja ko pt-br ru sr sv vi zh-cn zh-tw; do
    if [ `grep ^untranslated: content/$locale/docs/*.* | wc -l` -ge 18 ]; then
	docs_untranslated="$docs_untranslated $locale"
	continue
    fi
    po4a-gettextize --format text --option markdown --option yfm_keys=title \
		    --localized-charset=UTF-8 --master-charset=UTF-8 \
		    `for f in content/en/docs/*.*; do echo " --master $f "; done` \
		    `for f in content/$locale/docs/*.*; do echo " --localized $f "; done` \
		    --po po/docs.$locale.po \
	|| mv gettextization.failed.po po/docs.$locale.po
    if [ -e po/docs.$locale.po ]; then
	sed -i 's/^#, fuzzy/#/' po/docs.$locale.po
    fi
done

perl -i \
     -pe 'BEGIN{undef $/;} s/msgstr ""\n"#-#-#-#-#.*?\n\n/msgstr "{{< lastmod >}}\\n"\n\n/smg' \
     po/*.po

po4a --verbose po/docs.po4a.conf




pages_untranslated=""
for locale in de es fr id ja ko pt-br ru sr sv vi zh-cn zh-tw; do
    if [ `grep ^untranslated: content/$locale/*.* | wc -l` -ge 18 ]; then
	pages_untranslated="$pages_untranslated $locale"
	continue
    fi
    po4a-gettextize --format text --option markdown --option yfm_keys=title \
		    --localized-charset=UTF-8 --master-charset=UTF-8 \
		    `for f in content/en/*.*; do echo " --master $f "; done` \
		    `for f in content/$locale/*.*; do echo " --localized $f "; done` \
		    --po po/pages.$locale.po \
	|| mv gettextization.failed.po po/pages.$locale.po
    if [ -e po/pages.$locale.po ]; then
	sed -i 's/^#, fuzzy/#/' po/pages.$locale.po
    fi
done

po4a --verbose po/pages.po4a.conf


#msgid "{{< clientslastmod >}}\n"
#msgid "{{< /clients >}}\n"
#msgid "{{< docs_index >}}\n"
#msgid "  {{< donorbox >}}\n"
#msgid "  {{% fundraiser %}}\n"
#msgid "{{< i18n_status >}}\n"
#msgid "{{< lastmod >}}\n"
#msgid "{{< plotly >}}\n"
#msgid "{{< sponsors >}}\n"

perl -i \
     -pe 'BEGIN{undef $/;} s/msgid "( *{{[^}=]+}}\\n)"\nmsgstr ""\n\n/msgid "\1"\nmsgstr "\1"\n\n/smg' \
     po/*.po


echo "SUCCESS (untranslated docs: $docs_untranslated   pages: $pages_untranslated)"
